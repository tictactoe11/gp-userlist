// ==UserScript==
// @name        gp / userlist (bitbucket)
// @namespace   Violentmonkey Scripts
// @match       https://garticphone.com/*
// @grant       none
// @version     0.1
// @author      You
// @description 9/18/2021, 8:11:02 PM
// @run-at      document-start
// @downloadURL https://bitbucket.org/tictactoe11/gp-userlist/raw/master/src/userscript/gp-userlist.user.js
// ==/UserScript==

'use strict';

const BASE_URL = 'https://bitbucket.org/tictactoe11/gp-userlist/raw/master'
// const BASE_URL = 'https://localhost/test/gp-userlist'

const resources = {
  style: [
    'style/GPUserList.css',
  ],
  script: [
    'src/GPPacketParser.js',
    'src/GPProxy.js',
    'src/GPUserList.js',
    'src/app.js'
  ],
}

document.addEventListener('DOMContentLoaded', loadResources)

function loadResources() {
  // load .css
  resources.style.forEach((filename) => {
    const url = `${BASE_URL}/${filename}`
    fetch(url)
      .then(res => res.text())
      .then(css => {
        const style = document.createElement('style')
        style.innerHTML = css
        document.head.appendChild(style)
      })
  })

  // load .js
  resources.script.forEach((filename) => {
    const el = document.createElement('script');
    el.src = `${BASE_URL}/${filename}`
    el.setAttribute('defer', '')
    document.head.appendChild(el)
  })
}



// Class Overrides

class WebSocket1 extends WebSocket {
  constructor(...args) {
    super(...args)
    const ev = new CustomEvent('_ws', { detail: { ws: this } })
    document.dispatchEvent(ev)
  }

  send(...args) {
    const ev = new CustomEvent('_ws_send_data', { detail: { encodedPacket: args[0] } })
    document.dispatchEvent(ev)
    super.send(...args)
  }

  set onmessage(handler) {
    super.addEventListener('message', (e) => {
      document.dispatchEvent(new CustomEvent('_onmessage_intercept', { detail: { handler, e } }))
    })
  }
}

class XMLHttpRequest1 extends XMLHttpRequest {
  constructor(...args) {
    super(...args)

    this.addEventListener('load', (e) => {
      const ev = new CustomEvent('_xhr_data', { detail: { encodedPacket: this.responseText } })
      document.dispatchEvent(ev)
    })
  }
}

WebSocket = WebSocket1
XMLHttpRequest = XMLHttpRequest1
