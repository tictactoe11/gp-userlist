class GPPacketParser_ {
  constructor() {
    this.socketIOParser = new SocketIOParser_()
    this.engineIOClient = new EngineIOClient_()

    return {
      encodeString: this.socketIOParser.encodeString,
      add: this.socketIOParser.add,
      decodeString: this.socketIOParser.decodeString,
      decodePayload: this.engineIOClient.decodePayload,
      decode: this.decode.bind(this)
    }
  }

  decode(encodedPacket, isPollingData) {
    if (!isPollingData) {
      return this.socketIOParser.add(encodedPacket).data
    } else {
      const { type, data } = this.engineIOClient.decodePayload(encodedPacket)
      return (type === 'message') ? this.socketIOParser.add(data).data : null
    }
  }
}

class SocketIOParser_ {
  constructor() {

    var withNativeArrayBuffer = typeof ArrayBuffer === 'function';

    var isView = function (obj) {
      return typeof ArrayBuffer.isView === 'function' ? ArrayBuffer.isView(obj) : (obj.buffer instanceof ArrayBuffer);
    };

    function isBuf(obj) {
      return (withNativeArrayBuffer && (obj instanceof ArrayBuffer || isView(obj)));
    }
    const exports = {}

    exports.protocol = 4;

    exports.types = [
      'CONNECT',
      'DISCONNECT',
      'EVENT',
      'ACK',
      'ERROR',
      'BINARY_EVENT',
      'BINARY_ACK'
    ];

    exports.CONNECT = 0;
    exports.DISCONNECT = 1;
    exports.EVENT = 2;
    exports.ACK = 3;
    exports.ERROR = 4;
    exports.BINARY_EVENT = 5;
    exports.BINARY_ACK = 6;

    function encodeString(obj) {
      let str = ''
      let nsp = false

      str += obj.type

      if (exports.BINARY_EVENT == obj.type || exports.BINARY_ACK == obj.type) {
        str += obj.attachments
        str += '-'
      }

      if (obj.nsp && '/' != obj.nsp) {
        nsp = true
        str += obj.nsp
      }

      if (null != obj.id) {
        if (nsp) {
          str += ','
          nsp = false
        }
        str += obj.id
      }

      if (null != obj.data) {
        if (nsp) str += ',';
        str += JSON.stringify(obj.data)
      }

      return str
    }

    function add(obj) {
      var packet;
      if (typeof obj === 'string') {
        packet = decodeString(obj);
        if (exports.BINARY_EVENT === packet.type || exports.BINARY_ACK === packet.type) { // binary packet's json
          this.reconstructor = new BinaryReconstructor(packet);
          if (this.reconstructor.reconPack.attachments === 0) {
            return packet
          }
        } else {
          return packet
        }
      } else if (isBuf(obj) || obj.base64) { // raw binary data
        if (!this.reconstructor) {
          throw new Error('got binary data when not reconstructing a packet');
        } else {
          packet = this.reconstructor.takeBinaryData(obj);
          if (packet) {
            this.reconstructor = null;
            return packet
          }
        }
      } else {
        throw new Error('Unknown type: ' + obj);
      }
    };

    function decodeString(str) {
      var i = 0;
      var p = {
        type: Number(str.charAt(0))
      };

      if (null == exports.types[p.type]) {
        return error('unknown packet type ' + p.type);
      }

      if (exports.BINARY_EVENT === p.type || exports.BINARY_ACK === p.type) {
        var start = i + 1;
        while (str.charAt(++i) !== '-' && i != str.length) { }
        var buf = str.substring(start, i);
        if (buf != Number(buf) || str.charAt(i) !== '-') {
          throw new Error('Illegal attachments');
        }
        p.attachments = Number(buf);
      }

      if ('/' === str.charAt(i + 1)) {
        var start = i + 1;
        while (++i) {
          var c = str.charAt(i);
          if (',' === c) break;
          if (i === str.length) break;
        }
        p.nsp = str.substring(start, i);
      } else {
        p.nsp = '/';
      }

      var next = str.charAt(i + 1);
      if ('' !== next && Number(next) == next) {
        var start = i + 1;
        while (++i) {
          var c = str.charAt(i);
          if (null == c || Number(c) != c) {
            --i;
            break;
          }
          if (i === str.length) break;
        }
        p.id = Number(str.substring(start, i + 1));
      }

      if (str.charAt(++i)) {
        var payload = tryParse(str.substr(i));
        var isPayloadValid = payload !== false && (p.type === exports.ERROR || Array.isArray(payload));
        if (isPayloadValid) {
          p.data = payload;
        } else {
          return error('invalid payload');
        }
      }

      return p;
    }

    function tryParse(str) {
      try {
        return JSON.parse(str);
      } catch (e) {
        return false;
      }
    }

    function error(msg) {
      return {
        type: exports.ERROR,
        data: null
      };
    }

    return { add, decodeString, encodeString }
  }
}

class EngineIOClient_ {
  constructor() {
    var packets = {
      open: 0,
      close: 1,
      ping: 2,
      pong: 3,
      message: 4,
      upgrade: 5,
      noop: 6
    };

    var packetslist = Object.keys(packets);

    var err = { type: 'error', data: 'parser error' };

    function decodePacket(data, binaryType, utf8decode) {
      if (data === undefined) {
        return err;
      }
      // String data
      if (typeof data === 'string') {
        if (data.charAt(0) === 'b') {
          return decodeBase64Packet(data.substr(1), binaryType);
        }

        if (utf8decode) {
          data = tryDecode(data);
          if (data === false) {
            return err;
          }
        }
        var type = data.charAt(0);

        if (Number(type) != type || !packetslist[type]) {
          return err;
        }

        if (data.length > 1) {
          return { type: packetslist[type], data: data.substring(1) };
        } else {
          return { type: packetslist[type] };
        }
      }

      var asArray = new Uint8Array(data);
      var type = asArray[0];
      var rest = sliceBuffer(data, 1);
      if (Blob && binaryType === 'blob') {
        rest = new Blob([rest]);
      }
      return { type: packetslist[type], data: rest };
    }

    function tryDecode(data) {
      try {
        data = utf8.decode(data, { strict: false });
      } catch (e) {
        return false;
      }
      return data;
    }

    function decodeBase64Packet(msg, binaryType) {
      var type = packetslist[msg.charAt(0)];
      if (!base64encoder) {
        return { type: type, data: { base64: true, data: msg.substr(1) } };
      }

      var data = base64encoder.decode(msg.substr(1));

      if (binaryType === 'blob' && Blob) {
        data = new Blob([data]);
      }

      return { type: type, data: data };
    }

    function decodePayload(data, binaryType, callback) {
      if (typeof data !== 'string') {
        return decodePayloadAsBinary(data, binaryType, callback);
      }

      if (typeof binaryType === 'function') {
        callback = binaryType;
        binaryType = null;
      }

      var packet;
      if (data === '') {
        return err
      }

      var length = '', n, msg;

      for (var i = 0, l = data.length; i < l; i++) {
        var chr = data.charAt(i);

        if (chr !== ':') {
          length += chr;
          continue;
        }

        if (length === '' || (length != (n = Number(length)))) {
          return err
        }

        msg = data.substr(i + 1, n);

        if (length != msg.length) {
          return err
        }

        if (msg.length) {
          packet = decodePacket(msg, binaryType, false);

          if (err.type === packet.type && err.data === packet.data) {
            return err
          }

          return packet
          if (false === ret) return;
        }
        i += n;
        length = '';
      }

      if (length !== '') {
        return err
      }
    }

    function decodePayloadAsBinary(data, binaryType, callback) {
      if (typeof binaryType === 'function') {
        callback = binaryType;
        binaryType = null;
      }

      var bufferTail = data;
      var buffers = [];

      while (bufferTail.byteLength > 0) {
        var tailArray = new Uint8Array(bufferTail);
        var isString = tailArray[0] === 0;
        var msgLength = '';

        for (var i = 1; ; i++) {
          if (tailArray[i] === 255) break;

          if (msgLength.length > 310) {
            return callback(err, 0, 1);
          }

          msgLength += tailArray[i];
        }

        bufferTail = sliceBuffer(bufferTail, 2 + msgLength.length);
        msgLength = parseInt(msgLength);

        var msg = sliceBuffer(bufferTail, 0, msgLength);
        if (isString) {
          try {
            msg = String.fromCharCode.apply(null, new Uint8Array(msg));
          } catch (e) {
            var typed = new Uint8Array(msg);
            msg = '';
            for (var i = 0; i < typed.length; i++) {
              msg += String.fromCharCode(typed[i]);
            }
          }
        }

        buffers.push(msg);
        bufferTail = sliceBuffer(bufferTail, msgLength);
      }

      var total = buffers.length;
      buffers.forEach(function (buffer, i) {
        callback(exports.decodePacket(buffer, binaryType, true), i, total);
      });
    }

    return { decodePayload }
  }
}
