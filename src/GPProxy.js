class GPProxy_ extends EventTarget {
  static EVENTS = {
    GAME_QUIT: 28,
    CHANGE_GAME_MODE: 26,
    CHANGE_LOBBY_SCREEN: 27,
    CHANGE_GAME_SETTINGS: 18,
    GAME_STARTED: 5,
    DESCRIPTION_INPUT: 6,
    TOOL: 7,
    READY: 15,
    ALBUM_STARTED: 23,
    NEW_GAME: 20, // new lobby
    PLAYER_JOINED: 2,
    PLAYER_LEFT: 3, // from lobby
    PLAYER_KICKED: 14,
    PLAYER_LEFT_GAME: 21, // from game
    PLAYER_RETURNED: 22, // in game
    TURN_STARTED: 11,
    TURNS_ENDED: 24, // before 'album starts'
    TIMER_STARTED: 25,
    ALBUM_AUTHOR_INFO: 12,
    ALBUM_ITEM_DATA: 9,
    ALBUM_NEXT: 16,
    ALBUM_BACK: 17,
    ALBUM_SCORE: 35,
    NEXT_ROUND: 34, // score mode
  }

  static PAYLOAD_TYPES = {
    PRESETS: 1,
    DATA: 2,
  }

  static GAME_STAGES = {
    LOBBY: 1,
    GAME: 2,
  }

  static CENSORED_DRAW_PLACEHOLDER = [[3, 5, ["#a30000", 14, 1], [121, 221], [586, 50]], [3, 6, ["#a30000", 14, 1], [585, 50], [641, 190]], [3, 7, ["#a30000", 14, 1], [641, 191], [175, 364]], [3, 9, ["#a30000", 14, 1], [174, 364], [121, 223]], [3, 10, ["#a30000", 14, 1], [287, 269], [270, 221]], [3, 11, ["#a30000", 14, 1], [271, 221], [321, 254]], [3, 12, ["#a30000", 14, 1], [305, 208], [322, 255]], [3, 13, ["#a30000", 14, 1], [439, 160], [457, 209]], [3, 14, ["#a30000", 14, 1], [490, 139], [507, 187]], [3, 15, ["#a30000", 14, 1], [222, 239], [239, 284]], [3, 16, ["#a30000", 10, 1], [225, 235], [248, 228]], [3, 17, ["#a30000", 10, 1], [234, 259], [256, 251]], [3, 18, ["#a30000", 10, 1], [243, 284], [265, 276]], [3, 20, ["#a30000", 10, 1], [491, 139], [516, 130]], [3, 21, ["#a30000", 10, 1], [501, 164], [523, 154]], [3, 22, ["#a30000", 10, 1], [511, 186], [532, 179]], [3, 23, ["#a30000", 14, 1], [537, 125], [555, 170]], [3, 29, ["#a30000", 14, 1], [202, 242], [182, 250]], [3, 30, ["#a30000", 14, 1], [182, 250], [176, 271]], [3, 31, ["#a30000", 14, 1], [175, 272], [187, 294]], [3, 32, ["#a30000", 14, 1], [187, 294], [203, 296]], [3, 33, ["#a30000", 14, 1], [203, 296], [218, 291]], [3, 34, ["#a30000", 14, 1], [353, 188], [334, 197]], [3, 35, ["#a30000", 14, 1], [334, 197], [333, 213]], [3, 36, ["#a30000", 14, 1], [333, 213], [363, 221]], [3, 37, ["#a30000", 14, 1], [364, 222], [369, 233]], [3, 38, ["#a30000", 14, 1], [366, 237], [353, 245]], [3, 40, ["#a30000", 14, 1], [353, 245], [343, 245]], [3, 41, ["#a30000", 14, 1], [392, 174], [413, 173]], [3, 42, ["#a30000", 14, 1], [413, 173], [426, 192]], [3, 43, ["#a30000", 14, 1], [425, 193], [426, 216]], [3, 44, ["#a30000", 14, 1], [426, 216], [409, 225]], [3, 45, ["#a30000", 14, 1], [409, 225], [392, 221]], [3, 46, ["#a30000", 14, 1], [392, 221], [382, 200]], [3, 47, ["#a30000", 14, 1], [380, 199], [388, 178]], [3, 48, ["#a30000", 14, 1], [443, 156], [460, 151]], [3, 49, ["#a30000", 14, 1], [460, 151], [473, 157]], [3, 50, ["#a30000", 14, 1], [473, 157], [468, 173]], [3, 51, ["#a30000", 14, 1], [468, 173], [453, 179]], [3, 52, ["#a30000", 14, 1], [466, 177], [486, 194]], [3, 53, ["#a30000", 14, 1], [542, 121], [563, 116]], [3, 54, ["#a30000", 14, 1], [562, 116], [582, 123]], [3, 55, ["#a30000", 14, 1], [582, 124], [586, 141]], [3, 56, ["#a30000", 14, 1], [585, 146], [579, 160]], [3, 57, ["#a30000", 14, 1], [579, 160], [561, 168]]]

  constructor(ws) {
    super()

    this.ws
    this.user = {}
    this.users = []

    this.isAuthorized = false

    this.gameStage = GPProxy_.GAME_STAGES.LOBBY
    this.autoKickState = false

    this.wsMessageHandler = this.wsMessageHandler.bind(this)
    this.autoKickHandler = this.autoKickHandler.bind(this)

    this.parser = new GPPacketParser_()

    document.addEventListener('_xhr_data', ({ detail: { encodedPacket } }) => {
      const data = this.parser.decode(encodedPacket, true)
      if (data) {
        this.setPresets(data[1])
      }
    })

    document.addEventListener('_ws_send_data', ({ detail: { encodedPacket } }) => {
      if (encodedPacket && encodedPacket.length > 3) {
        const data = this.parser.decode(encodedPacket)
        if (data) {
          this.wsSendHandler(data)
        }
      }
    })

    document.addEventListener('_onmessage_intercept', ({ detail: { handler, e } }) => {
      // handler(this.banMiddleware(e))
      handler(e)
    })

    this.addEventListener('users-updated', this.autoKickHandler)
    this.addEventListener('banlist-updated', this.autoKickHandler)

    this.setWebSocket(ws)
  }

  setWebSocket(ws) {
    if (this.ws) {
      this.ws.removeEventListener('message', this.wsMessageHandler)
    }
    this.ws = ws
    this.ws.addEventListener('message', this.wsMessageHandler)
  }

  setPresets(data) {
    console.log('presets:', data)
    if (data.users) {
      this.user = data.user
      this.users = data.users
      this.users.find((u) => u.id === data.user.id).you = true
      this.isAuthorized = this.user.uid !== this.user.authId
      const ev = new CustomEvent('users-updated', { detail: { users: this.users } })
      this.dispatchEvent(ev)
    }
  }

  wsMessageHandler({ data: wsData }) {
    if (typeof wsData === 'string') {
      const decodedData = this.parser.decodeString(wsData).data
      if (decodedData) {
        const [, event, data] = decodedData
        this.commandHandler(event, data)
      }
    }
  }

  wsSendHandler(decodedData) {
    const [, event, data] = decodedData
    if (event === GPProxy_.EVENTS.CHANGE_GAME_SETTINGS) {
      this.commandHandler(event, data)
    }
  }

  commandHandler(event, data) {
    let isUsersUpdated = true

    const EVENT = GPProxy_.EVENTS
    switch (event) {
      case EVENT.NEW_GAME:
        this.users = this.users
          .filter((u) => {
            return !u.away
          })
          .map((u) => {
            u.viewer = undefined
            u.ready = false
            return u
          })
        this.gameStage = GPProxy_.GAME_STAGES.LOBBY
        break;
      case EVENT.ALBUM_SCORE:
        this.users = this.users.map((u) => {
          u.viewer = undefined
          return u
        })
        break
      case EVENT.TURN_STARTED:
        this.users = this.users.map((u) => {
          u.ready = false
          return u
        })
        break;
      case EVENT.PLAYER_JOINED:
        this.users.push(data)
        break;
      case EVENT.PLAYER_LEFT:
        var { userLeft, newOwner } = data
        this.users = this.users.filter((u) => u.id !== userLeft)
        if (newOwner) {
          this.user.owner = this.user.id === newOwner
          this.users = this.users.map((u) => {
            u.owner = u.id === newOwner
            return u
          })
        }
        break;
      case EVENT.PLAYER_LEFT_GAME:
        var { userLeft, newOwner } = data
        this.users = this.users.map((u) => {
          u.away = u.id === userLeft || u.away
          return u
        })
        if (newOwner) {
          this.user.owner = this.user.id === newOwner
          this.users = this.users.map((u) => {
            u.owner = u.id === newOwner
            return u
          })
        }
        break;
      case EVENT.PLAYER_RETURNED:
        this.users = this.users.map((u) => {
          u.away = u.away && u.id !== data
          return u
        })
        break;
      case EVENT.PLAYER_KICKED:
        this.users = this.users.filter((u) => u.id !== data)
        break;
      case EVENT.READY:
        var { user, ready } = data
        this.users = this.users.map((u) => {
          u.ready = u.id === user ? ready : u.ready
          return u
        })
        break;
      case EVENT.GAME_STARTED:
        this.gameStage = GPProxy_.GAME_STAGES.GAME
      default:
        isUsersUpdated = false
        break
    }

    if (isUsersUpdated) {
      const ev = new CustomEvent('users-updated', { detail: { users: this.users } })
      this.dispatchEvent(ev)
    }
  }

  send(command, data, type = 4, id = 2) {
    const packet = this.parser.encodeString({
      type, nsp: '/', id,
      data: [GPProxy_.PAYLOAD_TYPES.DATA, command, data]
    })
    this.ws.send(packet)
  }

  kickPlayer(id, nick) {
    this.send(GPProxy_.EVENTS.PLAYER_KICKED, id)
  }

  autoKickHandler() {
    if (!this.users || !this.user.owner || !this.autoKickState || this.gameStage !== GPProxy_.GAME_STAGES.LOBBY) return

    this.users.forEach((u) => {
      if (
        this.isUserBanned(u.nick) &&
        u.nick !== this.user.nick
      ) {
        this.kickPlayer(u.id, u.nick)
      }
    })
  }

  getUsers() {
    return this.users
  }

  setAutoKickState(state) {
    this.autoKickState = state
    if (state) {
      this.autoKickHandler()
    }
  }

  updateBanlist(banlist) {
    this.banlist = banlist
    this.dispatchEvent(new CustomEvent('banlist-updated'))
  }

  isUserBanned(username) {
    return this.banlist.has(username.toLowerCase())
  }

  banMiddleware(e) {
    if (e.data?.length > 3) {
      const packetData = this.parser.decode(e.data)
      if (packetData) {
        let isPacketChanged = false
        const [, event, data] = packetData

        const EVENT = GPProxy_.EVENTS
        switch (event) {
          case EVENT.ALBUM_AUTHOR_INFO:
            if (data?.bookAuthor && this.isUserBanned(data.bookAuthor.nick) && data.bookAuthor.id !== this.user.id) {
              packetData[2].bookAuthor.nick = 'CENSORED'
              isPacketChanged = true
            }
            break
          case EVENT.ALBUM_ITEM_DATA:
            if (data?.user && this.isUserBanned(data.user.nick) && data.user.id !== this.user.id) {
              packetData[2].user.nick = 'CENSORED'
              if (data.data) {
                packetData[2].data = data.type === 1 ? GPProxy_.CENSORED_DRAW_PLACEHOLDER : 'CENSORED' // types { 1: draw, 2: sentence }
              }
              isPacketChanged = true
            }
            break;
          case EVENT.TURN_STARTED:
            if (data?.previous?.user && this.isUserBanned(data.previous.user.nick) && data.previous.user.id !== this.user.id) {
              packetData[2].previous.user.nick = 'CENSORED'
              packetData[2].previous.data = data.previous.type === 1 ? GPProxy_.CENSORED_DRAW_PLACEHOLDER : 'CENSORED' // types { 1: draw, 2: sentence }
              isPacketChanged = true
            }
            break
        }

        if (isPacketChanged) {
          const encodedPacket = this.parser.encodeString({
            type: 4, nsp: '/', id: 2,
            data: packetData
          })

          return new MessageEvent('message', {
            data: encodedPacket,
            origin: e.origin,
            lastEventId: e.lastEventId,
            source: e.source,
            ports: e.ports,
          })
        }
      }
    }

    return e
  }
}
