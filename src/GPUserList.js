class GPUserList_ extends EventTarget {
  constructor() {
    super()

    this.list
    this.buttons

    this.users = []

    this.shownState = JSON.parse(localStorage.getItem('ul_shown')) || false
    this.compactMode = JSON.parse(localStorage.getItem('ul_compact_mode')) ?? true
    this.autoKickState = JSON.parse(localStorage.getItem('ul_autokick')) || false
    this.banlist = new Set(JSON.parse(localStorage.getItem('ul_banlist')) || [])

    this.usersClickHandler = this.usersClickHandler.bind(this)
    this.banlistClickHandler = this.banlistClickHandler.bind(this)

    this.addEventListener('shown-changed', ({ detail: { state } }) => {
      localStorage.setItem('ul_shown', JSON.stringify(state))
    })

    this.addEventListener('compact-mode-changed', ({ detail: { state } }) => {
      localStorage.setItem('ul_compact_mode', JSON.stringify(state))
    })

    this.addEventListener('autokick-changed', ({ detail: { state } }) => {
      localStorage.setItem('ul_autokick', JSON.stringify(state))
    })

    this.addEventListener('banlist-updated', (e) => {
      localStorage.setItem('ul_banlist', JSON.stringify(Array.from(this.banlist)))
    })

    this.render()
  }

  getElement() {
    return this.container
  }

  update(users) {
    this.users = users
    if (this.shownState) {
      this.renderUsers()
    }
  }

  render() {
    this.container = document.createElement('div')
    this.container.className = 'userlist_'
    this.container.classList.toggle('shown_', this.shownState)
    this.container.classList.toggle('compact_', this.compactMode)
    this.container.classList.toggle('autokick_', this.autoKickState)

    this.buttons = document.createElement('div')
    this.buttons.className = 'ul-buttons_'
    this.container.appendChild(this.buttons)

    this.renderBtn('activate', 'Скрыть/показать список игроков', (e) => {
      this.shownState = !this.shownState
      if (this.shownState) this.renderUsers()
      this.container.classList.toggle('shown_', this.shownState)
      this.container.classList.remove('banlist_')
      this.dispatchEvent(new CustomEvent('shown-changed', { detail: { state: this.shownState } }))
    })

    this.renderBtn('mode', 'Переключение между полным и компактным режимами', (e) => {
      this.compactMode = !this.compactMode
      this.container.classList.toggle('compact_', this.compactMode)
      this.dispatchEvent(new CustomEvent('compact-mode-changed', { detail: { state: this.compactMode } }))
    })

    // separator
    this.buttons.appendChild(document.createElement('div'))

    this.renderBtn('autokick', 'Автоматическое удаление забаненных игроков', (e) => {
      this.autoKickState = !this.autoKickState
      this.container.classList.toggle('autokick_', this.autoKickState)
      const ev = new CustomEvent('autokick-changed', {
        detail: {
          state: this.autoKickState,
          banlist: this.banlist
        }
      })
      this.dispatchEvent(ev)
    })

    this.renderBtn('banlist', 'Список забаненных игроков', (e) => {
      this.container.classList.toggle('banlist_')
    })

    this.list = document.createElement('div')
    this.list.className = 'ul-users_'
    this.list.addEventListener('dblclick', this.usersClickHandler)
    this.list.addEventListener('click', (e) => {
      if (!e.ctrlKey) return
      this.usersClickHandler(e)
    })
    this.container.appendChild(this.list)

    this.banlistElem = document.createElement('div')
    this.banlistElem.className = 'ul-banlist_'
    this.banlistElem.addEventListener('dblclick', this.banlistClickHandler)
    this.banlistElem.addEventListener('click', (e) => {
      if (!e.ctrlKey) return
      this.banlistClickHandler(e)
    })
    this.container.appendChild(this.banlistElem)

    this.renderBanlist()

    document.body.appendChild(this.container)
  }

  renderBanlist() {
    const scrollY = this.banlistElem.scrollTop
    this.banlistElem.innerHTML = ''
    const frag = document.createDocumentFragment()

    const title = document.createElement('span')
    title.className = 'ul-title_'
    title.textContent = 'Забаненные'
    frag.appendChild(title)

    if (this.banlist.size) {
      this.banlist.forEach((username) => {
        const el = document.createElement('div')
        el.className = 'ul-banned-user_'
        el.textContent = username
        el.dataset.username = username
        frag.appendChild(el)
      })
    } else {
      const placeholder = document.createElement('div')
      placeholder.className = 'ul-banlist-placeholder_'
      placeholder.textContent = 'Список пуст'
      frag.appendChild(placeholder)
    }

    this.banlistElem.appendChild(frag)
    this.banlistElem.scrollTop = scrollY
  }

  renderBtn(className, title, handler) {
    const btn = document.createElement('div')
    btn.className = `ul-button_ ul-${className}-btn_`
    btn.title = title || ''
    btn.addEventListener('click', handler)
    this.buttons.appendChild(btn)
    return btn
  }

  renderUsers(users = this.users) {
    const scrollY = this.list.scrollTop
    this.list.innerHTML = ''

    let isFirstViewer

    const frag = document.createDocumentFragment()
    users.forEach((u) => {
      if (u.viewer && !isFirstViewer) {
        isFirstViewer = true
        const separator = document.createElement('span')
        separator.className = 'ul-title_'
        separator.textContent = 'Ожидают'
        frag.appendChild(separator)
      }
      frag.appendChild(this.renderUser(u))
    })

    this.list.appendChild(frag)
    this.list.scrollTop = scrollY
  }

  renderUser(u) {
    const el = document.createElement('div')
    el.className = 'ul-user_'
    el.classList.toggle('owner_', Boolean(u.owner))
    el.classList.toggle('away_', Boolean(u.away))
    el.classList.toggle('ready_', Boolean(u.ready))
    el.classList.toggle('viewer_', Boolean(u.viewer))
    el.classList.toggle('you_', Boolean(u.you))
    el.classList.toggle('banned_', !Boolean(u.you) && this.isUserBanned(u.nick))
    el.dataset.username = u.nick
    el.innerHTML = `
    <div class="ul-av_"><span class="img_" style="background-image: url(/images/avatar/${u.avatar}.svg);"></span></div>
    <div class="ul-nick_" title="${u.nick}">${u.nick}</div>
    <div class="ul-state_"></div>
    `
    return el
  }

  isUserBanned(username) {
    return this.banlist.has(username.toLowerCase())
  }

  getBanlist() {
    return this.banlist
  }

  getAutoKickState() {
    return this.autoKickState
  }

  usersClickHandler(e) {
    const user = e.target.closest('.ul-user_')
    if (user && !user.classList.contains('you_')) {
      const newState = !user.classList.contains('banned_')
      user.classList.toggle('banned_', newState)
      const username = user.dataset.username.toLowerCase()
      if (newState) {
        this.banlist.add(username)
      } else {
        this.banlist.delete(username)
      }
      this.renderBanlist()
      this.dispatchEvent(new CustomEvent('banlist-updated', { detail: { banlist: this.banlist } }))
    }
  }

  banlistClickHandler(e) {
    const user = e.target.closest('.ul-banned-user_')
    if (user) {
      this.banlist.delete(user.dataset.username)
      this.renderBanlist()
      this.renderUsers()
      this.dispatchEvent(new CustomEvent('banlist-updated', { detail: { banlist: this.banlist } }))
    }
  }
}
