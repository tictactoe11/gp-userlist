(() => {
  let gpproxy, userlist

  document.addEventListener('_ws', ({ detail: { ws } }) => {
    if (!gpproxy) {
      init(ws)
    } else {
      gpproxy.setWebSocket(ws)
    }
  })

  function init(ws) {
    userlist = new GPUserList_()
    userlist.addEventListener('autokick-changed', ({ detail: { state } }) => {
      gpproxy.setAutoKickState(state)
    })
    userlist.addEventListener('banlist-updated', ({ detail: { banlist } }) => {
      gpproxy.updateBanlist(banlist)
    })

    gpproxy = new GPProxy_(ws)
    gpproxy.updateBanlist(userlist.getBanlist())
    gpproxy.setAutoKickState(userlist.getAutoKickState())
    gpproxy.addEventListener('users-updated', (e) => {
      userlist.update(e.detail.users)
    })
  }
})()
